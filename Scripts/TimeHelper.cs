/// <summary>
/// Contains static helper functions related to timekeeping.
/// </summary>
public static class TimeHelper
{
    /// <summary>
    /// Gets the current time stamp since Unix epoch (1970/1/1).
    /// </summary>
    /// <returns>Time in ms since Unix epoch.</returns>
    public static long GetTimeStamp()
    {
        // MS since Unix epoch
        TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1);
        long msSinceEpoch = (long)timeSpan.TotalMilliseconds;

        return msSinceEpoch;
    }
}