using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Various commonly useful extension methods for Unity development.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Calculates the squared magnitude of a vector between two positions.
    /// Since we avoid an expensive Sqrt call, this is more performant. <para />
    /// Useful for e.g. comparisons, but remember to take the square root of the value if you need the true distance.
    /// </summary>
    /// <param name="a">This position in space.</param>
    /// <param name="b">A position in space.</param>
    /// <returns>The squared length of a vector from this position to b.</returns>
    public static float SquaredDistanceTo(this Vector3 a, Vector3 b)
    {
        return (a - b).sqrMagnitude;
    }

    /// <summary>
    /// Shuffles this list randomly. Uses an implementation of the Fisher-Yates shuffle algorithm.
    /// </summary>
    /// <param name="list">List to shuffle.</param>
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        System.Random random = new System.Random();

        while (n > 1)
        {
            n--;
            int k = random.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    /// <summary>
    /// Calculates and returns the average position (arithmetic mean) from positions in this array.
    /// </summary>
    /// <returns>Average value.</returns>
    public static Vector3 Average(this Vector3[] array)
    {
        Vector3 averagePosition = Vector3.zero;

        for (int i = 0; i < array.Length; i++)
        {
            averagePosition += array[i];
        }

        return averagePosition / array.Length;
    }

    /// <summary>
    /// Removes a sequence of characters from the start of a string.
    /// </summary>
    /// <param name="value">Sequence of characters to remove.</param>
    /// <param name="comparisonType">Defines case / culture sensitivity.</param>
    /// <returns>Source string with value removed.</returns>
    public static string RemoveStart(this string inputText, string value, StringComparison comparisonType = StringComparison.CurrentCulture)
    {
        if (!string.IsNullOrEmpty(value))
        {
            while (!string.IsNullOrEmpty(inputText) && inputText.StartsWith(value, comparisonType))
            {
                inputText = inputText.Substring(value.Length);
            }
        }

        return inputText;
    }

    /// <summary>
    /// Removes a sequence of characters from the end of a string.
    /// </summary>
    /// <param name="value">Sequence of characters to remove.</param>
    /// <param name="comparisonType">Defines case / culture sensitivity.</param>
    /// <returns>Source string with value removed.</returns>
    public static string RemoveEnd(this string inputText, string value, StringComparison comparisonType = StringComparison.CurrentCulture)
    {
        if (!string.IsNullOrEmpty(value))
        {
            while (!string.IsNullOrEmpty(inputText) && inputText.EndsWith(value, comparisonType))
            {
                inputText = inputText.Substring(0, (inputText.Length - value.Length));
            }
        }

        return inputText;
    }

    /// <summary>
    /// Assigns the values from my optimized struct position to Unity's Vector3 position.
    /// </summary>
    public static Vector3 ToVector3(this PositionData.Position myPosition)
    {
        return new Vector3(myPosition.x, myPosition.y, myPosition.z);
    }
}
